# README #

This app displays a list of Pokémon paginated in 10 Pokémon at a time.

When you click on a Pokémon, its basic characteristics are displayed, as well as its type, abilities, statistics and games.

If you want, you can add a Pokémon to your favorites and this will also be shown in the initial list.

### Webhook ###

- https://webhook.site/96275c9b-8ba5-43d5-8760-1bd5147e699d

- https://webhook.site/#!/96275c9b-8ba5-43d5-8760-1bd5147e699d/f3549335-8c7a-4e49-ac6b-a26a36b546a9/1
