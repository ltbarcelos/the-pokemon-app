//
//  Pokedex_LucasBarcelosUITests.swift
//  Pokedex_LucasBarcelosUITests
//
//  Created by Lucas Barcelos on 10/04/20.
//  Copyright © 2020 Lucas Barcelos. All rights reserved.
//

import XCTest

class Pokedex_LucasBarcelosUITests: XCTestCase {

    var app: XCUIApplication!
    
    override func setUp() {
        super.setUp()
        continueAfterFailure = false
        app = XCUIApplication()
        app.launch()
    }

    override func tearDown() {
        app = nil
        super.tearDown()
    }
    
    func test_Favorite() {
        app.collectionViews.cells.otherElements.containing(.staticText, identifier:"Bulbasaur").element.tap()
        app.buttons["fav"].tap()
        app/*@START_MENU_TOKEN@*/.staticTexts["Types"]/*[[".buttons[\"Types\"].staticTexts[\"Types\"]",".staticTexts[\"Types\"]"],[[[-1,1],[-1,0]]],[0]]@END_MENU_TOKEN@*/.tap()
        app.navigationBars["Types"].buttons["Bulbasaur"].tap()
        app.buttons["Stats"].tap()
        app.navigationBars["Stats"].buttons["Bulbasaur"].tap()
        app.buttons["Games"].tap()
        app.navigationBars["Games"].buttons["Bulbasaur"].tap()
        app.buttons["Skills"].tap()
        app.navigationBars["Skills"].buttons["Bulbasaur"].tap()
        app.navigationBars["Bulbasaur"].buttons["Pokédex"].tap()
        
        XCTAssertNotNil(app.collectionViews.cells.otherElements.containing(.image, identifier:"fav_pressed").element, "Pokémon favorited")
    }
    
    func test_Search() {
        app.navigationBars["Pokédex"].searchFields["Pokémon name..."].tap()
        
        _ = app/*@START_MENU_TOKEN@*/.keys["C"]/*[[".keyboards.keys[\"C\"]",".keys[\"C\"]"],[[[-1,1],[-1,0]]],[0]]@END_MENU_TOKEN@*/
        _ = app/*@START_MENU_TOKEN@*/.keys["h"]/*[[".keyboards.keys[\"h\"]",".keys[\"h\"]"],[[[-1,1],[-1,0]]],[0]]@END_MENU_TOKEN@*/
        _ = app/*@START_MENU_TOKEN@*/.keys["a"]/*[[".keyboards.keys[\"a\"]",".keys[\"a\"]"],[[[-1,1],[-1,0]]],[0]]@END_MENU_TOKEN@*/
        
        XCTAssertNotNil(app.collectionViews.cells.otherElements.containing(.staticText, identifier:"Charmander").element.tap(), "Charmander selected")
    }
}
