//
//  PokemonListTests.swift
//  Pokedex_LucasBarcelosTests
//
//  Created by Lucas Barcelos on 10/04/20.
//  Copyright © 2020 Lucas Barcelos. All rights reserved.
//

import XCTest
@testable import Pokedex_LucasBarcelos

class PokemonListTests: XCTestCase {

    var pokemonList: PokemonListModel?

    override func setUp() {
        let bundle = Bundle(for: type(of: self))

        guard let url = bundle.url(forResource: "pokemonList", withExtension: "json") else {
            XCTFail("Missing file: list.json")
            return
        }

        guard let data = try? Data(contentsOf: url) else {
            XCTFail("Missing data")
            return
        }
        pokemonList = try? JSONDecoder().decode(PokemonListModel.self, from: data)
    }
    
    override func tearDown() {
         pokemonList = nil
     }

    func test_MissingResponse() {
        pokemonList = nil
        XCTAssert(pokemonList == nil)
    }
    
    func test_ListExist() {
        guard let pokemonList = pokemonList else { XCTFail("Missing list"); return }
        XCTAssert(pokemonList.results.count != 0)
    }
    
    func test_ListContainCharmander() {
        guard let pokemonList = pokemonList else { XCTFail("Missing list"); return }
        XCTAssert(pokemonList.results.contains(where: { (pokemon) -> Bool in
            if pokemon.name == "charmander" {
                return true
            }
            return false
        }))
    }
}
