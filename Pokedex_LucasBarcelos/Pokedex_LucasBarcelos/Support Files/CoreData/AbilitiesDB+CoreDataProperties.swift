//
//  AbilitiesDB+CoreDataProperties.swift
//  Pokedex_LucasBarcelos
//
//  Created by Lucas Barcelos on 16/04/20.
//  Copyright © 2020 Lucas Barcelos. All rights reserved.
//
//

import Foundation
import CoreData


extension AbilitiesDB {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<AbilitiesDB> {
        return NSFetchRequest<AbilitiesDB>(entityName: "AbilitiesDB")
    }

    @NSManaged public var is_hidden: Bool
    @NSManaged public var slot: Int64
    @NSManaged public var ability: ResultGenericDB?
    @NSManaged public var pokemonDetails: PokemonDetailsDB?

}
