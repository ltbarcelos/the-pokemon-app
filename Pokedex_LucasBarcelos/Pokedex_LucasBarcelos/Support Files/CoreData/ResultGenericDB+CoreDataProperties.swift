//
//  ResultGenericDB+CoreDataProperties.swift
//  Pokedex_LucasBarcelos
//
//  Created by Lucas Barcelos on 16/04/20.
//  Copyright © 2020 Lucas Barcelos. All rights reserved.
//
//

import Foundation
import CoreData


extension ResultGenericDB {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<ResultGenericDB> {
        return NSFetchRequest<ResultGenericDB>(entityName: "ResultGenericDB")
    }

    @NSManaged public var name: String?
    @NSManaged public var url: String?
    @NSManaged public var abilityResult: AbilitiesDB?
    @NSManaged public var statResult: StatsDB?
    @NSManaged public var typeResult: TypesDB?
    @NSManaged public var versionResult: GameIndicesDB?

}
