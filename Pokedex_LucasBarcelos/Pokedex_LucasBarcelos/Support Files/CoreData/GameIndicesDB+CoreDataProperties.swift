//
//  GameIndicesDB+CoreDataProperties.swift
//  Pokedex_LucasBarcelos
//
//  Created by Lucas Barcelos on 16/04/20.
//  Copyright © 2020 Lucas Barcelos. All rights reserved.
//
//

import Foundation
import CoreData


extension GameIndicesDB {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<GameIndicesDB> {
        return NSFetchRequest<GameIndicesDB>(entityName: "GameIndicesDB")
    }

    @NSManaged public var game_index: Int64
    @NSManaged public var pokemonDetails: PokemonDetailsDB?
    @NSManaged public var version: ResultGenericDB?

}
