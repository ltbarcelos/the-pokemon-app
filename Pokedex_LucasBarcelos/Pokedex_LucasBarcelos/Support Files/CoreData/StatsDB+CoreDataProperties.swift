//
//  StatsDB+CoreDataProperties.swift
//  Pokedex_LucasBarcelos
//
//  Created by Lucas Barcelos on 16/04/20.
//  Copyright © 2020 Lucas Barcelos. All rights reserved.
//
//

import Foundation
import CoreData


extension StatsDB {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<StatsDB> {
        return NSFetchRequest<StatsDB>(entityName: "StatsDB")
    }

    @NSManaged public var base_stat: Int64
    @NSManaged public var effort: Int64
    @NSManaged public var pokemonDetails: PokemonDetailsDB?
    @NSManaged public var stat: ResultGenericDB?

}
