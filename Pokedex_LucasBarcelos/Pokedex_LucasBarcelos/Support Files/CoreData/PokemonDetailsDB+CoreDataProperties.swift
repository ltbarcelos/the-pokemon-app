//
//  PokemonDetailsDB+CoreDataProperties.swift
//  Pokedex_LucasBarcelos
//
//  Created by Lucas Barcelos on 16/04/20.
//  Copyright © 2020 Lucas Barcelos. All rights reserved.
//
//

import Foundation
import CoreData


extension PokemonDetailsDB {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<PokemonDetailsDB> {
        return NSFetchRequest<PokemonDetailsDB>(entityName: "PokemonDetailsDB")
    }

    @NSManaged public var height: Int64
    @NSManaged public var id: Int64
    @NSManaged public var name: String?
    @NSManaged public var sprites: String?
    @NSManaged public var weight: Int64
    @NSManaged public var abilities: NSSet?
    @NSManaged public var game_indices: NSSet?
    @NSManaged public var stats: NSSet?
    @NSManaged public var types: NSSet?

}

// MARK: Generated accessors for abilities
extension PokemonDetailsDB {

    @objc(addAbilitiesObject:)
    @NSManaged public func addToAbilities(_ value: AbilitiesDB)

    @objc(removeAbilitiesObject:)
    @NSManaged public func removeFromAbilities(_ value: AbilitiesDB)

    @objc(addAbilities:)
    @NSManaged public func addToAbilities(_ values: NSSet)

    @objc(removeAbilities:)
    @NSManaged public func removeFromAbilities(_ values: NSSet)

}

// MARK: Generated accessors for game_indices
extension PokemonDetailsDB {

    @objc(addGame_indicesObject:)
    @NSManaged public func addToGame_indices(_ value: GameIndicesDB)

    @objc(removeGame_indicesObject:)
    @NSManaged public func removeFromGame_indices(_ value: GameIndicesDB)

    @objc(addGame_indices:)
    @NSManaged public func addToGame_indices(_ values: NSSet)

    @objc(removeGame_indices:)
    @NSManaged public func removeFromGame_indices(_ values: NSSet)

}

// MARK: Generated accessors for stats
extension PokemonDetailsDB {

    @objc(addStatsObject:)
    @NSManaged public func addToStats(_ value: StatsDB)

    @objc(removeStatsObject:)
    @NSManaged public func removeFromStats(_ value: StatsDB)

    @objc(addStats:)
    @NSManaged public func addToStats(_ values: NSSet)

    @objc(removeStats:)
    @NSManaged public func removeFromStats(_ values: NSSet)

}

// MARK: Generated accessors for types
extension PokemonDetailsDB {

    @objc(addTypesObject:)
    @NSManaged public func addToTypes(_ value: TypesDB)

    @objc(removeTypesObject:)
    @NSManaged public func removeFromTypes(_ value: TypesDB)

    @objc(addTypes:)
    @NSManaged public func addToTypes(_ values: NSSet)

    @objc(removeTypes:)
    @NSManaged public func removeFromTypes(_ values: NSSet)

}
