//
//  TypesDB+CoreDataProperties.swift
//  Pokedex_LucasBarcelos
//
//  Created by Lucas Barcelos on 16/04/20.
//  Copyright © 2020 Lucas Barcelos. All rights reserved.
//
//

import Foundation
import CoreData


extension TypesDB {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<TypesDB> {
        return NSFetchRequest<TypesDB>(entityName: "TypesDB")
    }

    @NSManaged public var slot: Int64
    @NSManaged public var pokemonDetails: PokemonDetailsDB?
    @NSManaged public var type: ResultGenericDB?

}
