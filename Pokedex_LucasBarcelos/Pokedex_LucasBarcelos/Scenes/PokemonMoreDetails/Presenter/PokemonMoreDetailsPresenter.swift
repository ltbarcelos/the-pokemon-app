//
//  PokemonMoreDetailsPresenter.swift
//  Pokedex_LucasBarcelos
//
//  Created by Lucas Barcelos on 12/04/20.
//  Copyright © 2020 Lucas Barcelos. All rights reserved.
//

import Foundation
import UIKit

protocol PokemonMoreDetailsPresenterDelegate: class {
    func setupDetails(identifier: String)
    func setupNavigation(category: String)
}

class PokemonMoreDetailsPresenter {
    
    // MARK: - Properties
    var moreDetails: PokemonDetailsModel?
    weak var delegate: PokemonMoreDetailsPresenterDelegate?
    var view: PokemonMoreDetailsVC?
    var identifier: String = ""
    
    // MARK: - View Life Cycle
    func viewDidLoad() {
        self.delegate?.setupDetails(identifier: identifier)
    }
    
    // MARK: - Methods
    func configureCellType(cell: PokemonTypeCell, for row: Int) {
        guard let details = moreDetails else { return }
        self.delegate?.setupNavigation(category: "Types")
        cell.displayTypeInformations(name: details.types[row].type.name)
    }
    
    func configureCellStats(cell: PokemonStatsCell, for row: Int) {
        guard let details = moreDetails else { return }
        self.delegate?.setupNavigation(category: "Stats")
        cell.displayStatsInformations(statsName: details.stats[row].stat.name, baseStat: String(describing: details.stats[row].base_stat), effort: String(describing: details.stats[row].effort))
    }
    
    func configureCellGames(cell: PokemonGamesCell, for row: Int) {
        guard let details = moreDetails else { return }
        self.delegate?.setupNavigation(category: "Games")
        cell.displayGamesInformations(gameName: details.game_indices[row].version.name, gameIndex: String(describing: details.game_indices[row].game_index))
    }
    
    func configureCellSkills(cell: PokemonSkillsCell, for row: Int) {
        guard let details = moreDetails else { return }
        self.delegate?.setupNavigation(category: "Skills")
        for ability in details.abilities {
            if !ability.is_hidden {
                cell.displaySkillsInformations(ability: details.abilities[row].ability.name)
            }
        }
    }
    
    func numberOfRows(identifier: String) -> Int {
        switch identifier {
        case "PokemonTypeCell":
            return moreDetails?.types.count ?? 0
        case "PokemonStatsCell":
            return moreDetails?.stats.count ?? 0
        case "PokemonGamesCell":
            return moreDetails?.game_indices.count ?? 0
        case "PokemonSkillsCell":
            return moreDetails?.abilities.count ?? 0
        default:
            return 0
        }
    }
}
