//
//  PokemonMoreDetailsVC.swift
//  Pokedex_LucasBarcelos
//
//  Created by Lucas Barcelos on 12/04/20.
//  Copyright © 2020 Lucas Barcelos. All rights reserved.
//

import Foundation
import UIKit

class PokemonMoreDetailsVC: UIViewController {

    // MARK: - Outlets
    @IBOutlet weak var tableView: UITableView!
    
    // MARK: - Properties
    var identifier = String()
    let presenter = PokemonMoreDetailsPresenter()
    
    // MARK: - View Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        presenter.delegate = self
        presenter.viewDidLoad()
        self.tableView.delegate = self
        self.tableView.dataSource = self
    }
}

extension PokemonMoreDetailsVC: PokemonMoreDetailsPresenterDelegate {
    func setupNavigation(category: String) {
        self.navigationItem.title = category.capitalizingFirstLetter()
    }
    
    func setupDetails(identifier: String) {
        self.identifier = identifier
    }
}

extension PokemonMoreDetailsVC: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let count = presenter.numberOfRows(identifier: self.identifier)
        return count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: self.identifier, for: indexPath)
        
        switch identifier {
        case "PokemonTypeCell":
            presenter.configureCellType(cell: cell as! PokemonTypeCell, for: indexPath.row)
        case "PokemonStatsCell":
            presenter.configureCellStats(cell: cell as! PokemonStatsCell, for: indexPath.row)
        case "PokemonGamesCell":
            presenter.configureCellGames(cell: cell as! PokemonGamesCell, for: indexPath.row)
        case "PokemonSkillsCell":
            presenter.configureCellSkills(cell: cell as! PokemonSkillsCell, for: indexPath.row)
        default:
            break
        }
        return cell
    }
}
