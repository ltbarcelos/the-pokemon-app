//
//  PokemonDetailsCellPresenter.swift
//  Pokedex_LucasBarcelos
//
//  Created by Lucas Barcelos on 12/04/20.
//  Copyright © 2020 Lucas Barcelos. All rights reserved.
//

import UIKit

@objc protocol PokemonDetailsCellPresenter: class {
    @objc optional func displayTypeInformations(name: String)
    @objc optional func displayStatsInformations(statsName: String, baseStat: String, effort: String)
    @objc optional func displayGamesInformations(gameName: String, gameIndex: String)
    @objc optional func displaySkillsInformations(ability: String)
}
