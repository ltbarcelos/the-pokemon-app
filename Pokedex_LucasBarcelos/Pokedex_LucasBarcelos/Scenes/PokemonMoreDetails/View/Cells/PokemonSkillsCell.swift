//
//  PokemonSkillsCell.swift
//  Pokedex_LucasBarcelos
//
//  Created by Lucas Barcelos on 12/04/20.
//  Copyright © 2020 Lucas Barcelos. All rights reserved.
//

import UIKit
import Foundation

class PokemonSkillsCell: UITableViewCell {

    // MARK: - Outlets
    @IBOutlet weak var abilityLbl: UILabel!
    @IBOutlet weak var view: UIView!
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
}

// MARK: - Extension - PokemonSkillsCell
extension PokemonSkillsCell: PokemonDetailsCellPresenter {
    func displaySkillsInformations(ability: String) {
        abilityLbl.text = ability
        view.layer.cornerRadius = 4.0
    }
}
