//
//  PokemonGamesCell.swift
//  Pokedex_LucasBarcelos
//
//  Created by Lucas Barcelos on 12/04/20.
//  Copyright © 2020 Lucas Barcelos. All rights reserved.
//

import UIKit
import Foundation

class PokemonGamesCell: UITableViewCell {

    // MARK: - Outlets
    @IBOutlet weak var gameIndexLbl: UILabel!
    @IBOutlet weak var gameNameLbl: UILabel!
    @IBOutlet weak var view: UIView!
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
}

// MARK: - Extension - PokemonGamesCell
extension PokemonGamesCell: PokemonDetailsCellPresenter {
    func displayGamesInformations(gameName: String, gameIndex: String) {
        gameNameLbl.text = gameName
        gameIndexLbl.text = "Game Index: \(gameIndex)"
        view.layer.cornerRadius = 4.0
    }
}
