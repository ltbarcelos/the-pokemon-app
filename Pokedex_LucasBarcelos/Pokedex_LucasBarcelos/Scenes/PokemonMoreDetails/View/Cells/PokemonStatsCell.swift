//
//  PokemonStatsCell.swift
//  Pokedex_LucasBarcelos
//
//  Created by Lucas Barcelos on 12/04/20.
//  Copyright © 2020 Lucas Barcelos. All rights reserved.
//

import UIKit
import Foundation

class PokemonStatsCell: UITableViewCell {

    // MARK: - Outlets
    @IBOutlet weak var statsNameLbl: UILabel!
    @IBOutlet weak var baseStatLbl: UILabel!
    @IBOutlet weak var effortLbl: UILabel!
    @IBOutlet weak var view: UIView!
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
}

// MARK: - Extension - PokemonStatsCell
extension PokemonStatsCell: PokemonDetailsCellPresenter {
    func displayStatsInformations(statsName: String, baseStat: String, effort: String) {
        statsNameLbl.text = statsName
        baseStatLbl.text = "Base Stat: \(baseStat)"
        effortLbl.text = "Effort: \(effort)"
        view.layer.cornerRadius = 4.0
    }
}
