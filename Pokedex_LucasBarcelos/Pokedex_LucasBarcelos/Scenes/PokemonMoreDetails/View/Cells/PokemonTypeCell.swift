//
//  PokemonTypeCell.swift
//  Pokedex_LucasBarcelos
//
//  Created by Lucas Barcelos on 12/04/20.
//  Copyright © 2020 Lucas Barcelos. All rights reserved.
//

import UIKit
import Foundation

class PokemonTypeCell: UITableViewCell {

    // MARK: - Outlets
    @IBOutlet weak var typeNameLbl: UILabel!
    @IBOutlet weak var view: UIView!
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
}

// MARK: - Extension - PokemonTypeCell
extension PokemonTypeCell: PokemonDetailsCellPresenter {
    func displayTypeInformations(name: String) {
        typeNameLbl.text = name
        view.layer.cornerRadius = 4.0
    }
}
