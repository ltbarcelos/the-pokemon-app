//
//  PokemonListPresenter.swift
//  Pokedex_LucasBarcelos
//
//  Created by Lucas Barcelos on 10/04/20.
//  Copyright © 2020 Lucas Barcelos. All rights reserved.
//

import UIKit
import CoreData

protocol PokemonListPresenterDelegate: class {
    func startLoading()
    func stopLoading()
    func reloadData()
}

class PokemonListPresenter {
    
    // MARK: - Properties
    var listLoaded: [Results] = []
    var filtered: [Results] = []
    private var page: Int = 0
    var isFiltered: Bool = false
    let searchController = UISearchController(searchResultsController: nil)
    weak var delegate: PokemonListPresenterDelegate?
    
    // MARK: - View Life Cycle
    func viewDidLoad() {
        delegate?.startLoading()
        getList(pag: page)
    }
    
    func viewWillAppear() {
        delegate?.reloadData()
    }
    
    // MARK: - Methods
    func configureCell(_ cell: ListCellPresenter, for row: Int) {
        if isFiltered {
            let pokemonFavorite = CoreDataStack.getFavorite(name: filtered[row].name)
            cell.displayInformations(name: filtered[row].name, image: getPokemonID(url: filtered[row].url), isFavorite: pokemonFavorite)
        } else {
            let pokemonFavorite = CoreDataStack.getFavorite(name: listLoaded[row].name)
            cell.displayInformations(name: listLoaded[row].name, image: getPokemonID(url: listLoaded[row].url), isFavorite: pokemonFavorite)
        }
    }
    
    func getList(pag: Int) {
        let funcSuccess = { (item:PokemonListModel) -> Void  in
            DispatchQueue.main.async {
                self.delegate?.stopLoading()
                self.listLoaded += item.results
                self.delegate?.reloadData()
            }
        }
        
        let funcError = { (item:Bool) -> Void in
            if item {
                self.delegate?.stopLoading()
                let alert = UIAlertController(title: "Attention", message: "List not loaded", preferredStyle: .alert)
                let action = UIAlertAction(title: "Ok", style: .default, handler: nil)
                alert.addAction(action)
            }
        }
        PokemonNetworkManager.getList(nextPage: pag, funcSuccess: funcSuccess, funcError: funcError)
    }
    
    func getMorePokemon() {
        self.delegate?.startLoading()
        self.page += 10
        DispatchQueue.global().async {
            self.getList(pag: self.page)
        }
    }
    
    func getPokemonID(url: String) -> String {
        let arr = url.split(separator: "/")
        var pokemonID = String()
        if let last = arr.last {
            pokemonID = String(last)
        }
        return pokemonID
    }
    
    func filtered(_ searchText: String, scope: String = "All") {
        if searchText != "" {
            isFiltered = true
            filtered = listLoaded.filter { item in
                return (item.name.lowercased().contains(searchText.lowercased()))
            }
        } else {
            isFiltered = false
            filtered = listLoaded
        }
        delegate?.reloadData()
    }
}
