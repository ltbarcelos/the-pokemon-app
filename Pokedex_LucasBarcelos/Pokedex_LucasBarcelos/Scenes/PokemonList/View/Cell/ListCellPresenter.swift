//
//  ListCellPresenter.swift
//  Pokedex_LucasBarcelos
//
//  Created by Lucas Barcelos on 10/04/20.
//  Copyright © 2020 Lucas Barcelos. All rights reserved.
//

import UIKit

protocol ListCellPresenter: class {
    func displayInformations(name: String, image: String, isFavorite: Bool)
}
