//
//  ListCell.swift
//  Pokedex_LucasBarcelos
//
//  Created by Lucas Barcelos on 10/04/20.
//  Copyright © 2020 Lucas Barcelos. All rights reserved.
//

import UIKit

class ListCell: UICollectionViewCell {
    
    // MARK: - Outlets
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var pokemonImage: DownloadImageView!
    @IBOutlet weak var favoriteImage: UIImageView!
    
    required init?(coder aDecoder: NSCoder) {
      super.init(coder: aDecoder)
      
      layer.cornerRadius = 5.0
    }
    
    override func becomeFirstResponder() -> Bool {
        return true
    }
}

// MARK: - Extension - ListCellPresenter
extension ListCell: ListCellPresenter {
    func displayInformations(name: String, image: String, isFavorite: Bool) {
        titleLabel.text = name.capitalizingFirstLetter()
        pokemonImage.setUrl(ServiceParameter.urlPokemonImage.replacingOccurrences(of: "PARAM", with: image))
        
        if isFavorite {
            self.favoriteImage.image = #imageLiteral(resourceName: "fav_pressed")
        } else {
            self.favoriteImage.image = nil
        }
    }
}
