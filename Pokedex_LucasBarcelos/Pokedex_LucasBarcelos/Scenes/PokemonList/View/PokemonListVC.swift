//
//  PokemonListVC.swift
//  Pokedex_LucasBarcelos
//
//  Created by Lucas Barcelos on 10/04/20.
//  Copyright © 2020 Lucas Barcelos. All rights reserved.
//

import UIKit

class PokemonListVC: UIViewController {

    // MARK: - Outlets
    @IBOutlet weak var collectionView: UICollectionView!
    
    // MARK: - Properties
    var activityIndicator: UIActivityIndicatorView!
    let presenter = PokemonListPresenter()
    let searchController = UISearchController(searchResultsController: nil)
    
    // MARK: - View Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        presenter.delegate = self
        presenter.viewDidLoad()
        self.collectionView.delegate = self
        self.collectionView.dataSource = self
                
        if #available(iOS 11.0, *) {
            navigationItem.searchController = searchController
            navigationItem.hidesSearchBarWhenScrolling = false
        } else {
            navigationItem.titleView = searchController.searchBar
        }
        configureSearch()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        presenter.delegate = self
        presenter.viewWillAppear()
    }

    // MARK: - Methods
    private func configureSearch() {
        searchController.searchResultsUpdater = self
        searchController.obscuresBackgroundDuringPresentation = false
        searchController.searchBar.placeholder = "Pokémon name..."
        searchController.searchBar.tintColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        definesPresentationContext = true
    }
    
    // Mark: - Navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let cell = sender as? ListCell {
            let vc = segue.destination as? PokemonDetailsVC
            
            for id in presenter.listLoaded {
                if id.name == cell.titleLabel.text?.lowercased() {
                    vc?.pokemonID = Int(presenter.getPokemonID(url: id.url)) ?? 0
                }
            }
        }
    }
}

extension PokemonListVC: UICollectionViewDelegate, UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "PokeCell", for: indexPath) as! ListCell
        presenter.configureCell(cell, for: indexPath.row)
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if searchController.isActive {
            return presenter.filtered.count
        } else {
            return presenter.listLoaded.count
        }
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
      return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        if indexPath.row == presenter.listLoaded.count - 2 {
            presenter.getMorePokemon()
        }
    }
}

extension PokemonListVC: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let width = (self.view.frame.size.width - 50) / 2
        let height = width * 1.5
        return CGSize(width: width, height: height)
    }
}

extension PokemonListVC: PokemonListPresenterDelegate {
    func startLoading() {
        DispatchQueue.main.async { [weak self] in
            self?.activityIndicator = UIActivityIndicatorView(style: .whiteLarge)
            self?.activityIndicator.color = #colorLiteral(red: 1, green: 0.1491314173, blue: 0, alpha: 1)
            self?.activityIndicator.startAnimating()
            self?.activityIndicator.isHidden = false
            self?.collectionView.backgroundView = self?.activityIndicator
        }
    }
    
    func stopLoading() {
        DispatchQueue.main.async { [weak self] in
            self?.activityIndicator.stopAnimating()
            self?.activityIndicator.isHidden = true
            self?.collectionView.backgroundView = nil
        }
    }
    
    func reloadData() {
        DispatchQueue.main.async { [weak self] in
            self?.collectionView.reloadData()
        }
    }
}

extension PokemonListVC: UISearchResultsUpdating {
    func updateSearchResults(for searchController: UISearchController) {
        if let text = searchController.searchBar.text {
            presenter.filtered(text)
        }
    }
}
