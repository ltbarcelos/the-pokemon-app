//
//  PokemonDetailsPresenter.swift
//  Pokedex_LucasBarcelos
//
//  Created by Lucas Barcelos on 11/04/20.
//  Copyright © 2020 Lucas Barcelos. All rights reserved.
//

import Foundation
import UIKit
import CoreData

protocol PokemonDetailsPresenterDelegate: class {
    func startLoading()
    func stopLoading()
    func setupDetails(details: PokemonDetailsModel)
    func setupFavorite(isFavorite:Bool, pokemon: PokemonDetailsModel)
    func configurePokemonImage()
    func configureCards(views: [UIView])
    func configureButtons(buttons: [UIButton])
}

class PokemonDetailsPresenter {
    
    // MARK: - Properties
    var detailsLoaded: PokemonDetailsModel?
    weak var delegate: PokemonDetailsPresenterDelegate?
    var view: PokemonDetailsVC?
    var pokemonDB = [PokemonDetailsDB]()
    let entityPokemonDetails = NSEntityDescription.entity(forEntityName: "PokemonDetailsDB", in: CoreDataStack.managedObjectContext)
    let entityAbilitiesDB = NSEntityDescription.entity(forEntityName: "AbilitiesDB", in: CoreDataStack.managedObjectContext)
    let entityGameIndicesDB = NSEntityDescription.entity(forEntityName: "GameIndicesDB", in: CoreDataStack.managedObjectContext)
    let entityStatsDB = NSEntityDescription.entity(forEntityName: "StatsDB", in: CoreDataStack.managedObjectContext)
    let entityTypesDB = NSEntityDescription.entity(forEntityName: "TypesDB", in: CoreDataStack.managedObjectContext)
    
    // MARK: - View Life Cycle
    func viewDidLoad(id: Int) {
        self.delegate?.startLoading()
        getDetails(id: id)
    }
    
    // MARK: - Methods
    func getDetails(id: Int) {
        let funcSuccess = { (item:PokemonDetailsModel) -> Void  in
            DispatchQueue.main.async {
                self.delegate?.stopLoading()
                self.detailsLoaded = item
                self.delegate?.setupDetails(details: item)
            }
        }
        
        let funcError = { (item:Bool) -> Void in
            if item {
                self.delegate?.stopLoading()
                let alert = UIAlertController(title: "Attention", message: "Details not loaded", preferredStyle: .alert)
                let action = UIAlertAction(title: "Ok", style: .default, handler: nil)
                alert.addAction(action)
            }
        }
        PokemonNetworkManager.getPokemonDetails(pokemonID: id, funcSuccess: funcSuccess, funcError: funcError)
    }
    
    func populateDB() {
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "PokemonDetailsDB")
        do {
            pokemonDB = try CoreDataStack.managedObjectContext.fetch(fetchRequest) as! [PokemonDetailsDB]
        } catch {}
    }
    
    func removePokemonCoreData(pokemonFavorite: PokemonDetailsModel) {
        self.populateDB()
        var index: Int = 0
        for pokemon in self.pokemonDB {
            if pokemon.name == pokemonFavorite.name {
                let context:NSManagedObjectContext = CoreDataStack.managedObjectContext
                context.delete(self.pokemonDB[index] as NSManagedObject)
                self.pokemonDB.remove(at: index)
                CoreDataStack.saveContext()
                self.delegate?.setupFavorite(isFavorite: false, pokemon: pokemonFavorite)
                return
            }
            index += 1
        }
    }
    
    func postFavorite(pokemon: PokemonDetailsModel) {
        let pokemonFavorite = CoreDataStack.getFavorite(name: pokemon.name)
        
        if pokemonFavorite {
            removePokemonCoreData(pokemonFavorite: pokemon)
        } else {
            self.setCoreData(pokemon: pokemon)
            let funcSuccess = { (item:Bool) -> Void  in
                DispatchQueue.main.async {
                    self.delegate?.stopLoading()
                    self.delegate?.setupFavorite(isFavorite: item, pokemon: pokemon)
                }
            }
            
            let funcError = { (item:Bool) -> Void in
                if item {
                    self.delegate?.stopLoading()
                    let alert = UIAlertController(title: "Attention", message: "Favorite error", preferredStyle: .alert)
                    let action = UIAlertAction(title: "Ok", style: .default, handler: nil)
                    alert.addAction(action)
                }
            }
            PokemonNetworkManager.postFavorite(pokemon: pokemon, funcSuccess: funcSuccess, funcError: funcError)
        }
    }
    
    func configureView(_ view: PokemonDetailsVC, for model: PokemonDetailsModel) {
        let pokemonFavorite = CoreDataStack.getFavorite(name: model.name)
        
        view.nameLbl.text = "\(model.name.capitalizingFirstLetter() + " - #\(model.id)")"
        view.heightLbl.text = "\(model.height)"
        view.weightLbl.text = "\(model.weight)"
        view.pokemonImage.setUrl(model.sprites.front_default)
        view.navigationItem.title = model.name.capitalizingFirstLetter()
        
        pokemonFavorite ? view.favoriteBtn.setImage(#imageLiteral(resourceName: "fav_pressed"), for: .normal) : view.favoriteBtn.setImage(#imageLiteral(resourceName: "fav"), for: .normal)
        
        self.delegate?.configurePokemonImage()
        self.delegate?.configureCards(views: [view.buttonsCardView, view.infoCardView])
        self.delegate?.configureButtons(buttons: view.moreDetailsBtn)
    }
    
    func setCoreData(pokemon: PokemonDetailsModel) {
        guard let entityPokemon = self.entityPokemonDetails,
        let entityAbilities = self.entityAbilitiesDB,
        let entityGameIndices = self.entityGameIndicesDB,
        let entityStatsDB = self.entityStatsDB,
        let entityTypesDB = self.entityTypesDB else { return }
        
        // MARK: - Set DBs
        let abilitiesDB = AbilitiesDB(entity: entityAbilities, insertInto: CoreDataStack.managedObjectContext)
        let gameIndicesDB = GameIndicesDB(entity: entityGameIndices, insertInto: CoreDataStack.managedObjectContext)
        let statsDB = StatsDB(entity: entityStatsDB, insertInto: CoreDataStack.managedObjectContext)
        let typesDB = TypesDB(entity: entityTypesDB, insertInto: CoreDataStack.managedObjectContext)
        let pokemonDB = PokemonDetailsDB(entity: entityPokemon, insertInto: CoreDataStack.managedObjectContext)
        
        // Abilities DB
        for abilitie in pokemon.abilities {
            abilitiesDB.is_hidden = abilitie.is_hidden
            abilitiesDB.slot = Int64(abilitie.slot)
            abilitiesDB.ability?.name = abilitie.ability.name
            abilitiesDB.ability?.url = abilitie.ability.url
        }
        
        // GameIndices DB
        for game in pokemon.game_indices {
            gameIndicesDB.game_index = Int64(game.game_index)
            gameIndicesDB.version?.name = game.version.name
            gameIndicesDB.version?.url = game.version.url
        }
        
        // Stats DB
        for stat in pokemon.stats {
            statsDB.base_stat = Int64(stat.base_stat)
            statsDB.effort = Int64(stat.effort)
            statsDB.stat?.name = stat.stat.name
            statsDB.stat?.url = stat.stat.url
        }
        
        // Types DB
        for type in pokemon.types {
            typesDB.slot = Int64(type.slot)
            typesDB.type?.name = type.type.name
            typesDB.type?.url = type.type.url
        }
        
        pokemonDB.name = pokemon.name
        pokemonDB.id = Int64(pokemon.id)
        pokemonDB.height = Int64(pokemon.height)
        pokemonDB.weight = Int64(pokemon.weight)
        pokemonDB.sprites = pokemon.sprites.front_default
        
        pokemonDB.addToAbilities(abilitiesDB)
        pokemonDB.addToGame_indices(gameIndicesDB)
        pokemonDB.addToStats(statsDB)
        pokemonDB.addToTypes(typesDB)
        
        CoreDataStack.saveContext()
    }
}
