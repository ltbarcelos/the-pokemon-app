//
//  PokemonDetailsModel.swift
//  Pokedex_LucasBarcelos
//
//  Created by Lucas Barcelos on 11/04/20.
//  Copyright © 2020 Lucas Barcelos. All rights reserved.
//

import UIKit
import Foundation

class PokemonDetailsModel: Codable {
    var sprites: Sprites
    var id, height, weight: Int
    var name: String
    var types: [Types]
    var stats: [Stats]
    var game_indices: [GameIndices]
    var abilities: [Abilities]
    
    enum CodingKeys: String, CodingKey {
        case sprites
        case id
        case name
        case height
        case weight
        case types
        case stats
        case game_indices
        case abilities
    }
}

class Abilities: Codable {
    var ability: Results
    var is_hidden: Bool
    var slot: Int
    
    enum CodingKeys: String, CodingKey {
        case ability
        case is_hidden
        case slot
    }
}

class GameIndices: Codable {
    var game_index: Int
    var version: Results
    
    enum CodingKeys: String, CodingKey {
        case game_index
        case version
    }
}

class Sprites: Codable {
    var front_default: String
    
    enum CodingKeys: String, CodingKey {
        case front_default
    }
}

class Stats: Codable {
    var base_stat, effort: Int
    var stat: Results
    
    enum CodingKeys: String, CodingKey {
        case base_stat
        case effort
        case stat
    }
}

class Types: Codable {
    var slot: Int
    var type: Results
    
    enum CodingKeys: String, CodingKey {
        case slot
        case type
    }
}
