//
//  PokemonDetailsVC.swift
//  Pokedex_LucasBarcelos
//
//  Created by Lucas Barcelos on 11/04/20.
//  Copyright © 2020 Lucas Barcelos. All rights reserved.
//

import UIKit

class PokemonDetailsVC: UIViewController {

    // MARK: - Outlets
    @IBOutlet weak var nameLbl: UILabel!
    @IBOutlet weak var heightLbl: UILabel!
    @IBOutlet weak var weightLbl: UILabel!
    @IBOutlet weak var pokemonImage: DownloadImageView!
    @IBOutlet weak var infoCardView: UIView!
    @IBOutlet weak var buttonsCardView: UIView!
    @IBOutlet var moreDetailsBtn: [UIButton]!
    @IBOutlet weak var favoriteBtn: UIButton!
    
    // MARK: - Properties
    var activityIndicator: UIActivityIndicatorView!
    let presenter = PokemonDetailsPresenter()
    var thisPokemon: PokemonDetailsModel?
    var pokemonID = Int()
    
    // MARK: - View Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        presenter.delegate = self
        presenter.viewDidLoad(id: pokemonID)
    }
    
    // Mark: - Navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let vc = segue.destination as? PokemonMoreDetailsVC {
            switch segue.identifier {
            case "moreDetailsType":
                vc.presenter.identifier = "PokemonTypeCell"
            case "moreDetailsStats":
                vc.presenter.identifier = "PokemonStatsCell"
            case "moreDetailsGames":
                vc.presenter.identifier = "PokemonGamesCell"
            case "moreDetailsSkills":
                vc.presenter.identifier = "PokemonSkillsCell"
            default:
                break
            }
            vc.presenter.moreDetails = presenter.detailsLoaded
        }
    }
    
    // MARK: - Actions
    @IBAction func favoriteBtn(_ sender: Any) {
        guard let pokemon = self.thisPokemon else { return }
        presenter.postFavorite(pokemon: pokemon)
    }
}

extension PokemonDetailsVC: PokemonDetailsPresenterDelegate {
    func setupFavorite(isFavorite: Bool, pokemon: PokemonDetailsModel) {
        isFavorite ? self.favoriteBtn.setImage(#imageLiteral(resourceName: "fav_pressed"), for: .normal) : self.favoriteBtn.setImage(#imageLiteral(resourceName: "fav"), for: .normal)
    }
    
    func configureButtons(buttons: [UIButton]) {
        for button in buttons {
            button.layer.cornerRadius = 4.0
            button.layer.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 0.5)
            button.clipsToBounds = true
        }
    }
    
    func configureCards(views: [UIView]) {
        for view in views {
            view.layer.cornerRadius = 8.0
            view.layer.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 0.4)
            view.clipsToBounds = true
        }
    }
    
    func configurePokemonImage() {
        self.pokemonImage.layer.cornerRadius = (self.pokemonImage.frame.size.width) / 2
        self.pokemonImage.layer.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 0.4)
        self.pokemonImage.clipsToBounds = true
    }
    
    func setupDetails(details: PokemonDetailsModel) {
        self.thisPokemon = details
        presenter.configureView(self, for: details)
    }
    
    func startLoading() {
        DispatchQueue.main.async { [weak self] in
            self?.activityIndicator = UIActivityIndicatorView(style: .whiteLarge)
            self?.activityIndicator.color = #colorLiteral(red: 1, green: 0.1491314173, blue: 0, alpha: 1)
            self?.activityIndicator.startAnimating()
            self?.activityIndicator.isHidden = false
        }
    }
    
    func stopLoading() {
        DispatchQueue.main.async { [weak self] in
            self?.activityIndicator.stopAnimating()
            self?.activityIndicator.isHidden = true
        }
    }
}

enum SelectedButtonTag: Int {
    case First
    case Second
    case Third
    case Fourth
}
