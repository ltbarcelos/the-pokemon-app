//
//  String+CapitalizeFirstLetter.swift
//  Pokedex_LucasBarcelos
//
//  Created by Lucas Barcelos on 11/04/20.
//  Copyright © 2020 Lucas Barcelos. All rights reserved.
//

import Foundation
import UIKit

extension String {
    func capitalizingFirstLetter() -> String {
        return prefix(1).capitalized + dropFirst()
    }

    mutating func capitalizeFirstLetter() {
        self = self.capitalizingFirstLetter()
    }
}
