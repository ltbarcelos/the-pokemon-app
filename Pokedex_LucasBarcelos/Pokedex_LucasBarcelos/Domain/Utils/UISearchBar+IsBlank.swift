//
//  UISearchBar+IsBlank.swift
//  Pokedex_LucasBarcelos
//
//  Created by Lucas Barcelos on 11/04/20.
//  Copyright © 2020 Lucas Barcelos. All rights reserved.
//

import Foundation
import UIKit

extension UISearchBar {
    
    func isBlank() -> Bool {
        var result: Bool = false
        
        if text == nil || text == "" {
            result = true
        }
        return result
    }
}
