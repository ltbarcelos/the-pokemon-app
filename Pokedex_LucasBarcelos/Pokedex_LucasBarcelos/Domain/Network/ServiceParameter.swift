//
//  ServiceParameter.swift
//  Pokedex_LucasBarcelos
//
//  Created by Lucas Barcelos on 10/04/20.
//  Copyright © 2020 Lucas Barcelos. All rights reserved.
//

import Foundation

class ServiceParameter {
    
    // Endpoint API
    static let urlAPI: String = "https://pokeapi.co/api/v2/pokemon/"
    static let urlPokemonImage: String = "https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/PARAM.png"
    static let urlFavorite: String = "https://webhook.site/96275c9b-8ba5-43d5-8760-1bd5147e699d"
    
}
