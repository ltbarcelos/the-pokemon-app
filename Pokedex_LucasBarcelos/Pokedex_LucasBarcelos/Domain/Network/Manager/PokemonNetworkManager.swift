//
//  PokemonListManager.swift
//  Pokedex_LucasBarcelos
//
//  Created by Lucas Barcelos on 10/04/20.
//  Copyright © 2020 Lucas Barcelos. All rights reserved.
//

import Foundation

class PokemonNetworkManager {
    
    // MARK: - Consult List
    class func getList(nextPage: Int?, funcSuccess: @escaping (_ status: PokemonListModel) -> Void, funcError: @escaping (_ status: Bool) -> Void) {
        
        // Endpoint API
        var endpoint = "\(ServiceParameter.urlAPI)?offset=PARAM&limit=10"
        
        guard let nextPage = nextPage else { return }
        
        endpoint = endpoint.replacingOccurrences(of: "PARAM", with: String(nextPage))
        
        guard let url = URL(string: endpoint) else {
            print("Error")
            return
        }
        let urlRequest = URLRequest(url: url)
        
        let config = URLSessionConfiguration.default
        let session = URLSession(configuration: config)
        
        let task = session.dataTask(with: urlRequest) {
            (data, response, error) in
            
            guard error == nil else {
                print("Error: Calling GET method")
                print(error!)
                return
            }
            
            guard let responseData = data else {
                print("Error: Did not receive data!")
                return
            }
            
            guard let httpResponse = response as? HTTPURLResponse else {
                print("Error: Did not receive response!")
                return
            }
            
            if httpResponse.statusCode == 200 {
                do {
                    let list = try JSONDecoder().decode(PokemonListModel.self, from: responseData)
                    funcSuccess(list)
                } catch  {
                    print("error trying to convert data to JSON")
                    return
                }
            } else {
                funcError(true)
            }
        }
        task.resume()
    }
    
    // MARK: - Consult Details
    class func getPokemonDetails(pokemonID: Int, funcSuccess: @escaping (_ status: PokemonDetailsModel) -> Void, funcError: @escaping (_ status: Bool) -> Void) {
        
        // Endpoint API
        let endpoint = "\(ServiceParameter.urlAPI)\(pokemonID)"
        
        guard let url = URL(string: endpoint) else {
            print("Error")
            return
        }
        let urlRequest = URLRequest(url: url)
        
        let config = URLSessionConfiguration.default
        let session = URLSession(configuration: config)
        
        let task = session.dataTask(with: urlRequest) {
            (data, response, error) in
            
            guard error == nil else {
                print("Error: Calling GET method")
                print(error!)
                return
            }
            
            guard let responseData = data else {
                print("Error: Did not receive data!")
                return
            }
            
            guard let httpResponse = response as? HTTPURLResponse else {
                print("Error: Did not receive response!")
                return
            }
            
            if httpResponse.statusCode == 200 {
                do {
                    let detail = try JSONDecoder().decode(PokemonDetailsModel.self, from: responseData)
                    funcSuccess(detail)
                } catch  {
                    print("error trying to convert data to JSON")
                    return
                }
            } else {
                funcError(true)
            }
        }
        task.resume()
    }
    
    // MARK: - Favorite
    class func postFavorite(pokemon: PokemonDetailsModel, funcSuccess: @escaping (_ status: Bool) -> Void, funcError: @escaping (_ status: Bool) -> Void) {
        
        let jsonData = try! JSONEncoder().encode(pokemon)
        var jsonString = String(data: jsonData, encoding: .utf8)!
        jsonString = jsonString.replacingOccurrences(of: "\"", with: "")
        
        let endpoint = "\(ServiceParameter.urlFavorite)"
        
        guard let url = URL(string: endpoint) else {
            print("Error")
            return
        }
        var urlRequest = URLRequest(url: url)
        urlRequest.httpMethod = "POST"
        urlRequest.httpBody = jsonData

        let task = URLSession.shared.dataTask(with: urlRequest) { data, response, error in
            guard let data = data else { return }
            let responseJSON = try? JSONSerialization.jsonObject(with: data, options: [])
            if let responseJSON = responseJSON as? [String: Any] {
                print(responseJSON)
            }
            
            error == nil ? funcSuccess(true) : funcError(true)
            
        }

        task.resume()
    }
}
